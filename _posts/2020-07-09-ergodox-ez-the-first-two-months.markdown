---
layout: post
title:  "Ergodox EZ&#58; the first two months"
date:   2020-07-09 08:03:00
categories: jekyll update
---

This February I felt a twinge of pain in my right wrist. The last time this had happened was in high school, when I strained my wrist while practicing the piano five hours a day and ended up wearing a wrist brace for the rest of the year. But I wasn't at the piano much in February, so this was something different.

It turns out that a couple of things were causing the pain. The first was my keyboard setup: a lot of typing on a stock Microsoft board perched on a desk-mounted keyboard tray that required me to tuck my elbows in to avoid my chair's armrests. At home, I found myself contorting my hands to fit a MacBook Pro keyboard. I'd tended to brush off the resulting minor aches and pains, figuring they would disappear with time. But this time they didn't.

The second cause was _how_ I was typing. I'd actually never learned to touch type, instead developing an unholy version of an [angle mod][angle] by seriously fudging the fingering for nearly half the keys on the board. Despite having practiced piano scales (with the correct fingering!) for hundreds of hours, I almost never used my pinkies on the computer keyboard keys they were supposed to hit, and I adopted strange tricks like sliding a finger across keys in one smooth movement as a speed boost.[^1] While the system worked for me, it came at a cost. Although I was able to work at 110 to 120 words per minute on a good day, I began to find that my fingers would tense up as I typed, meaning I couldn't always get into a [flow state][flow]. And extensive work in R and R Markdown, with their abundance of backticks and underscores, meant that my wrists had to move to accommodate my rigid-finger typing system.

[^1]: I mostly used this trick on diagonal key pairs, like _rd_ on a QWERTY layout. So I'd type _hard_ like so: _h_ with the right index finger, _a_ with the left ring finger, then _rd_ by hitting _r_ and sliding downward to _d_ with the left index finger. Probably doesn't work on a non-chiclet keyboard though.

I'd recognized these problems for some time but in February I actually started looking at more properly ergonomic setups. I briefly tried a Kinesis Freestyle, which was generally fine, but the lack of a numpad bothered me. Then the pandemic hit, and after a couple of weeks of shoving my wrists together on a laptop keyboard as part of a hastily assembled work-from-home setup, I decided to get serious about improving my work setup.

## Making the (key)switch

I ordered an [Ergodox EZ][ergodox] in early April and had it on my desk in under two weeks. I'd never used a mechanical keyboard long-term, so I opted for fairly safe Cherry Red keyswitches, and sprang for the glow model with a tent kit. The first two challenges I encountered were the ortholinear keys[^11] and the unusual placement of modifiers. The latter was quick to fix by playing with the Ergodox's built-in [Oryx graphical configurator][oryx]. I started with a vanilla layout, then tried moving frequently used whitespace and text removal keys [to the thumb clusters][layout1]. I also made an Oryx macro to generate the R `magrittr` pipe (`%>%`) from an inner 1u or 1.5u key.[^2]

[^11]: Technically the Ergodox's keys are not _ortholinear_ but rather _weakly column-staggered_. But practically speaking, I still think of and refer to the Ergodox as ortholinear, especially when comparing to something with a heavier column-stagger like the [Kyria][kyria].
[^2]: I'd forgotten that RStudio has a keyboard shortcut, `Ctrl + Shift + M`, to produce the `magrittr` pipe, so I removed that macro from the board a few weeks later. One lesson I'm still learning about custom key layouts is _bringing the keys to you_, rather than sticking stuff on hard-to-reach edge keys. I now use those inner 1.5u keys to toggle layers (`TG`).

The ortholinear keys were more challenging to adapt to. Unlearning my angle mod hacks was slow going, but I managed to hit 40 or 50 wpm on the Ergodox by the end of week 1. That's when I decided to make a far more fundamental switch: Colemak. One of my main reasons for choosing Colemak over, say, Dvorak, was that I wanted to avoid as much finger travel as humanly possible—less finger movement would also mean less wrist movement. I also thought it useful to keep frequently used punctuation and keyboard shortcut keys (period, comma, A, Z, X, C and V) in the same place, and in this respect Colemak seemed a happy medium between ease of adoption, portability and efficiency.[^3]

[^3]: Some of Martin Krzywinski's [carpalx layouts][carpalx] retain QWERTY-style punctuation and keyboard shortcut key placement, and are probably objectively better for weak pinkies compared to Colemak, which prioritizes finger travel at the expense of weak finger usage. I wanted to start with a simple layout that had reasonable market penetration before testing more customized layouts. But I'm not writing off something like QGMLWY just yet!

I did come to discover that it's pretty annoying to type single-finger bigrams on an ortholinear board, especially when you're used to sliding your finger across the keys so easily. I was lucky to have chosen a layout that tries to minimize such bigrams, at least relative to QWERTY and Dvorak. I think there's still room for improvement here, probably most easily through the Colemak-DH mod.

I was also keen to retain QWERTY fluency, since Colemak isn't supported on Windows by default, and since I never knew when I might find myself at a QWERTY board. My solution was to keep my MacBook on QWERTY and build a custom Colemak layer on the Ergodox. With this approach, progress was slower than if I'd jumped to Colemak cold turkey, but I've managed to retain QWERTY proficiency since day one of Colemak, albeit at a slower maximum wpm. I used the usual suspects to teach myself Colemak:  [learncolemak.com][learncolemak], [colemak.com][colemak.com], [keybr][keybr] and [keyhero][keyhero]. Within a couple of weeks, I was able to match my QWERTY speeds on the Ergodox, and by the second month I was hitting 80wpm on speed tests. Not quite up to my old QWERTY scores, but close enough to use full-time at work. Writing R in Colemak isn't nearly as bad as I thought it would be, even after accounting for the `tidyverse` muscle memory I'd developed over the past couple of years.

## Tweaking my layout

Editing key layouts has been largely iterative. My biggest design decisions have been enabling Auto Shift, using the thumb clusters for `Ctrl`, `Alt`, `Cmd` and `Win`, and adapting [DreymaR's Extend layer][extend]. I really like Auto Shift because it means I rarely have to pinky-reach for the Shift keys, but as a result I'm not able to adopt layouts that use held home row keys as modifiers. There's definitely an unresolved tension between maximizing thumb cluster and home row usage—I generally like the thumb clusters, but I have trouble reaching some of the top 1u thumb keys without moving my hand, and I've not yet settled on useful modifiers for those spots.[^4] As a result, I've stolen much of the bottom row for `MO` and `OSL` layer switches, which largely defeats the purpose of keeping modifiers off the home row. I suspect I'll have to revisit this design choice.

[^4]: I've been fiddling with the lighting a lot, so one of the top 1u keys on the right thumb cluster is currently a dedicated lighting on-off switch. On the one hand, that feels like a waste of valuable thumb cluster space, but on the other, I can hardly reach the key without taking my fingers off home row, so it wouldn't be viable as a modifier anyway. Others have noted [similar quibbles with the thumb cluster][thumbcluster], and while it's far from enough to sour my experience with the Ergodox, I still don't have a good plan for those keys.

I also went back and forth on Caps Lock. In my mind, there are four choices: Caps, Backspace, Escape or `MO` to Extend. I discarded Caps and Backspace due to underuse and my choice to put Backspace on the right thumb cluster, respectively. I've got a couple keys that `MO` to Extend elsewhere, so I went with Escape. However, I've found I hit it quite frequently when reaching for `Tab`, and I'm not a vim guy to begin with. Something else to revisit.

There's a lot left to do, and even more to learn. I'm much less ambivalent about Extend, the functionality of which is pretty incredible. The challenge will be to internalize these features, as I've got quite a bit of muscle memory to unlearn. So far, I really like the scroll wheel and browse backward and forward keys. And I haven't even touched the function layer, which will be another short-term cognitive burden with an appreciable long-term gain. I suspect that work will open up entirely new main layer possibilities. Still another thing on my radar is switching to a [QMK keymap][qmk] instead of Oryx to take advantage of [tap dancing][tapdance].

![My most recent Ergodox layout](/assets/ergodox-ez-take-one-2020-07-02.png)

[Here's][newlayout] my most recent layout. It's sure to change over the coming weeks and months, and there's a lot of redundancy to address, but the tinkerer in me looks forward to it!

<hr>

[ergodox]: https://ergodox-ez.com
[oryx]: https://configure.ergodox-ez.com
[layout1]: https://configure.ergodox-ez.com/ergodox-ez/layouts/e94Ry/XEDw6/0
[kinesis]: https://kinesis-ergo.com/shop/freestyle2-for-pc-us/
[angle]: https://colemakmods.github.io/ergonomic-mods/angle.html
[flow]: https://en.wikipedia.org/wiki/Flow_(psychology)
[qmk]: https://gitlab.com/danielspracklin/ergodox-ez/-/blob/master/keymap.c
[learncolemak]: https://www.learncolemak.com
[colemak.com]: https://colemak.com/Typing_lessons
[keybr]: https://www.keybr.com
[keyhero]: https://www.keyhero.com
[carpalx]: http://mkweb.bcgsc.ca/carpalx/?full_optimization
[extend]: https://forum.colemak.com/topic/2014-extend-extra-extreme/
[tapdance]: https://thomasbaart.nl/2018/12/13/qmk-basics-tap-dance/
[newlayout]: https://configure.ergodox-ez.com/ergodox-ez/layouts/e94Ry/latest/0
[thumbcluster]: https://www.reddit.com/r/MechanicalKeyboards/comments/9rpd9k/ergodox_say_something_bad_about_it/
[kyria]: https://blog.splitkb.com/blog/introducing-the-kyria

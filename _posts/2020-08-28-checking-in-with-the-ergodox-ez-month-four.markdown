---
layout: post
title:  "Checking in with the Ergodox EZ&#58; month 4"
date:   2020-08-28 09:23:00
categories: jekyll update
---

I've been using an Ergodox EZ for almost four months now. Long story short, everything is smoother than it was [two months ago](https://danielspracklin.gitlab.io/jekyll/update/2020/07/09/ergodox-ez-the-first-two-months.html), but there's a lot left to optimize.

## Extend and other layers

If there's one complaint I have about the Ergodox EZ, it's the sheer number of keys. I still don't have a concrete plan for half of the thumb cluster,[^1] the inner `1.5u` keys, or much of the bottom row. This boils down to continued indecision about typing philosophy: should I stuff the base layer full of things, or go the spartan route and consign less-used keys to higher layers? I'm leaning toward minimalism but have yet to fully commit.

[^1]: The recently unveiled [Moonlander](https://www.zsa.io/moonlander/) is pretty enviable in this regard, but I'm also got an eye on the [Kyria](https://splitkb.com).

That's a small quibble though, and on the whole I'm really enjoying iteratively improving my layers. The major change since month 2 has been moving Colemak to the base layer and removing the `TG` triggers from the inner columns; now, layer switching always starts with the thumb clusters or bottom row.

![My new base layer](/assets/ergodox-ez-take-one-base-2020-08-28.png)

On the function layer, I've mapped a shifted version of the numpad to the left hand. This has remained mostly unused so far, but I hope to incorporate it into my programming routine at some point. Another thing I'm keen to try is ditching the number row and relying on my function layer's numpad. The functionality's been there since day 1; I just need to convince myself to switch cold turkey. I may end up removing the numbers from the base layer entirely, although I suppose that could make some passwords harder to type.[^2]

[^2]: At a certain point, removing _too_ much from the board will frustrate the purpose of using an Ergodox. Some part of me thinks I'd be better off keeping the Ergodox as a maximalist board and building a Kyria or a Gergoplex to indulge my stripped-down whims.

![The function layer](/assets/ergodox-ez-take-one-fn-2020-08-28.png)


The Extend layer has proven to be invaluable. Mass-selecting and copying or deleting text is easy with `Extend-A-[IJKL]-[CP]` (_A_ maps to _Shift_, _C_ maps to _Copy_, and _P_ maps to _Delete_), and the Extend versions of cut, copy and paste are actually faster than holding the left thumb cluster for 150 milliseconds to trigger `Ctrl`. I've also implemented mouse control on the Extend layer, though I don't have a real use case for it yet.

![The Extend layer](/assets/ergodox-ez-take-one-extend-2020-08-28.png)

One of Oryx's newest features is the ability to easily pass alt-code text with the "Hold Alt pressed" option. This is a boon for those situations where I'm typing in French. Before this feature, I had to hit `Alt-MO2-1-3-0` to produce _é_; now it's as simple as `MO4-e`. I'm not yet sure where to place all the accented _e_ variants, but there's plenty of room on my fourth layer to experiment.

## Colemak

At this point I'm very close to my pre-Ergodox QWERTY speed of 120 words per minute, and I've developed enough Colemak muscle memory to blaze through simple text with a burst speed of 130–140 wpm. I've also taken to using [Monkey Type](https://monkey-type.com) on Master mode, which forces a restart with every mistake. The restart itself isn't particularly helpful,[^3] but it serves as a reminder to prioritize accuracy, not absolute speed. A couple days ago I tried a QWERTY Monkey Type test on my MacBook Pro, and it finally struck me how _much_ my hands had to move. QWERTY really is an unoptimized layout: what is _J_ doing in home row anyway?

[^3]: In fact, Master mode is arguably _detrimental_, since you never get the chance to identify and correct your mistakes.

With mastery comes the desire to beat a new challenge, and accordingly I've been mulling over DH and DHm. I don't find the _HE_ bigram terribly difficult to execute on vanilla Colemak,[^4] but I've definitely noticed that I type _the_ and _and_ more slowly than I would on a mod. The optimization space is more constrained now that I've applied the easiest fix by ditching QWERTY, but there's still room to improve. And that's the ultimate point of the Ergodox: iteratively building a layout that's perfect for me.

[^4]: I have no hard evidence to support this assertion, but I tend to find words containing _p_ and _b_ (think _public_) the trickiest to type. It's all very strange to me, since _b_ doesn't move on Colemak.

<hr>

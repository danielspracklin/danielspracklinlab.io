---
layout: post
title:  "My coffee setup"
date:   2020-09-12 18:34:00
categories: jekyll update
---

I'm interrupting my semi-regularly scheduled Ergodox programming with a post about something else that lends itself to constant tinkering: coffee!

## Beans

* **Beans**. I've been drinking coffee since my undergraduate days (instant Nescafé and Starbucks when I could afford it), but nowadays I almost exclusively use beans from [Little Victories Coffee Roasters](https://www.lvcoffee.ca). They do a lot of nice third-wave light roasts, and frequently source interesting single origin beans. Right now I'm drinking two Colombians: a very funky washed [Pink Bourbon](https://library.sweetmarias.com/glossary/pink-bourbon/) and a sugar process decaf from the Amigos del Huila collective. They come in standard 340-gram bags that I typically power through in ten to fourteen days, so I don't worry so much about air exposure. However, I do keep them away from sunlight or areas prone to temperature fluctuations.
* **Storage**. I store two more 340-gram bags in an Airscape Kilo. I just throw the bags themselves in, so I don't have to worry about commingling beans. Because I'm not storing my in-use bags in the Airscape, I open the container only every couple of weeks, which I hope impedes oxidation. The Kilo displaces air nicely,[^1] and while I haven't noticed a marked change in taste since its purchase, it's certainly nicer—and nicer looking—than nothing at all.

[^1]: I've heard that vacuum canisters are even better for coffee storage, though I haven't tested any. If you want to dive headlong down the rabbit hole, popular coffee YouTuber James Hoffmann has posted an [extremely thorough review of coffee storage methods](https://www.youtube.com/watch?v=K0JWuhE8a-w) that's well worth your time.

## Brewing

* **Grinder**. I've used several grinders over the years, starting with a Hario Skerton and Hario Mini Mill.[^2] The idea was to stick with manual grinders that played nicely with my preferred brewing methods, and that I could easily shuttle between the office and home. But once I moved to espresso, I grew dissatisfied with the grind consistency, and made the switch to a Comandante C-40 with the Red Clix espresso axle. Out of the box, the Comandante is a 30-micron stepped grinder, which in my tests was fine for AeroPress brews, but it didn't always give me the flexibility I wanted for espresso. On the other hand, the Red Clix axle's step distance of 15 microns is more than precise enough to dial in espresso.

    I thought about getting a non-manual grinder back in March, but discarded the idea largely due to cost: an electric grinder equivalent in quality to the Comandante could easily be twice the price. On these terms, the convenience of a 15-second grind just isn't worth it, and at any rate I've come to enjoy the tactile experience of grinding beans myself.
* **Brewers**. I use an AeroPress and a Flair Pro 2.
  - The Flair Pro 2 is my kind of espresso "machine": it's cheap,[^3] relatively portable, and it produces tasty and endlessly tweakable shots. By directly exposing water temperature, pressure and brew time to the user, it turns espresso into a puzzle with virtually endless combinations.[^4] The pro model's pressure gauge is pretty nice too. I typically aim for a 40- to 45-second extraction.
  - The Aeropress is reserved for times when I'm lazy, travelling, or drinking older beans. I use the standard method and typically brew fauxspresso: fine grind, pour just-off-the-boil water up to the `2` marker, wait 15 seconds for bloom, then push. It's nearly impossible to mess up, so long as you don't accidentally knock the thing over.
* **Water**. Ottawa's water is soft[^5] and pleasantly drinkable. I run it straight from the tap into a boring electric kettle.
* **Glasses**. Nothing fancy here, just a couple of double-walled glass espresso cups that show off the crema quite nicely.
* **Scale**. I have a simple scale that measures to the nearest tenth of a gram. I don't use it much now that I've dialed in my espresso, but it's there if I need it.

[^2]: Today I use the Skerton to grind peppercorns and other spices, as it's so easy to wash the ceramic burrs.
[^3]: At least in a world where dropping five figures on a La Marzocco is not only an option but an actively tempting one.
[^4]: The joy of solving puzzles that I'm describing is more or less straight out of Rands' blog post about [the "Nerd Handbook"](https://randsinrepose.com/archives/the-nerd-handbook/).
[^5]: In 2019, Ottawa's water had [under 30 mg/L of CaCO<sub>3</sub>](https://documents.ottawa.ca/sites/documents/files/2019_wq_summary_lemieux_island.pdf).

## What's missing?

As I've pointed out above, espresso is more or less infinitely customizable, even with a top-of-the-line machine that gets pressure and temperature just right every time. Although on a very basic level I've figured out how to pull decent espresso with the tools at my disposal, I know I could do _so much better_. How to get there? The most natural idea is with data. Ideally I'd have a simple, non-frictiony way to capture the technical specs of a shot (water temperature, pressure, brew time, grind size and consistency, dose, etc.), measure the extraction and the [TDS](https://en.wikipedia.org/wiki/Coffee_extraction), and record the subjective stuff: notes, acidity, mouthfeel, and so on.

The problem is that there's no easy way to do this. Some smart scales have Bluetooth-enabled apps that capture basic input data, but from what I've seen the apps tend to be buggy and not terribly user-friendly. Collecting the more technical data typically requires specialized equipment; even outside of the chemistry lab, data-driven espresso machines like the [Decent](https://decentespresso.com) fetch a hefty price. But while the technical data is beyond me at this point, I still think there's got to be an easier way to collect at least some of this stuff, and thereby improve the quality of my espresso. We'll see what I come up with!

<hr>

---
layout: post
title:  "Ergodox EZ month 5&#58; ctrl issues"
date:   2020-09-23 08:29:00
categories: jekyll update
---

_This is the third entry in an ongoing series chronicling my adventures with the Ergodox EZ keyboard. Months 1 and 2 are covered [here](https://danielspracklin.gitlab.io/jekyll/update/2020/07/09/ergodox-ez-the-first-two-months.html), and months 3 and 4 [here](https://danielspracklin.gitlab.io/jekyll/update/2020/08/28/checking-in-with-the-ergodox-ez-month-four.html)._

To what extent are ergonomic keyboards so named because they slow your typing speed to hand-friendly levels? That question has been the theme of month 5 with the Ergodox EZ.

## Control

On my main layer I've mapped space to the left `2u` key on the left-hand board. Holding the key for a user-customizable length of time modifies it to left Control; in QMK-speak this is `LCTL_T(KC_SPACE)`.

![`LCTL_T(KC_SPACE)` in the Oryx graphical configurator](/assets/ergodoz-ez-lctl-t-kc-space.png)

This configuration is great at reducing hand motion—no more pinky-stretching to the far left of the keyboard. But I've discovered that it comes at a price: over the past month I've accidentally hit space instead of Control-something several times a day. It happens when saving documents, selecting a body of text, running code, opening new tabs, you name it. While these aren't catastrophic issues, it is annoying to see all of your code disappear in a flash, and doubly annoying to have to undo them with the very same Control key that caused the problem.

I thought that I might have been jumping the gun on the subsequent keystroke, so I tried decreasing the auto shift timeout to 135 milliseconds. But this hasn't substantially decreased my Control error rate so far. Further, around the same time as the Control issue, I experienced some bouncing on the `KC_SPACE` key. This seems to have been resolved by increasing the debounce delay to 35 milliseconds, so I'm not sure that it's related, but I tend to be wary when two things go wrong at the same time.

So while I can't know for sure if it's mere impatience or something gone awry, either way I'm re-evaluating the idea of overloading keys with modifiers. I think there are four reasonable solutions here.

- **Further decrease auto shift timeout**. I'm now [experimenting](https://gitlab.com/danielspracklin/ergodox-ez/-/commit/91b0ecdeea7abf0edea91ba51dadf4a5206c3252) with an auto shift timeout of 125 milliseconds. I'm not sure whether a 10-millisecond difference falls above or below the [temporal just-noticeable difference](https://en.wikipedia.org/wiki/Just-noticeable_difference), but consider this an impromptu experiment!
- **Move Control to the Extend layer**. In fact I've already mapped Control to Extend, on the Colemak `s` key (`KC_LCTRL` on `KC_S`); it's just a matter of using it consistently. There's no auto shift delay to reach the Extend layer, so the extra key press to get there might actually save me some time. However, it means I won't be able to use this approach to save my work, as the `s` key will have been overwritten. I would need to create a one-shot Extend layer to surmount this,[^1] or create a "save my work" macro key and plunk it somewhere on the bottom row. I'll call this one a strong _maybe_.
- **Move Control elsewhere on L0**. I've got five empty keys on the bottom row, and another five on the thumb clusters. I could easily give Control a dedicated key. However, the thumb cluster keys don't seem ideal here, because I'd have to shift my entire hand to move my thumb high enough. The bottom-row keys are better candidates, but then my setup would be no better than the non-ergonomic one that was giving me pinky pain. So this isn't my preferred solution, but I'll keep it in my back pocket.
- **Change modifier philosophies**. I've been playing with [Miryoku](https://github.com/manna-harbour/miryoku), a minimalist layout that moves modifiers to the home row. This approach wouldn't solve the auto shift issue, but removing modifiers from the frequently-used `KC_SPACE` key could help. Whether this is a viable alternative over the long term remains to be seen.

[^1]: I'd have a similar problem with selecting all text, as Extend-Shift is mapped to the same key as Colemak `a`.

As always, experimenting with the Ergodox EZ raises more questions than answers; we shall see what month 6 brings.

<hr>

---
layout: post
title:  "Ways to demonstrate the CLT with purrr"
date:   2020-10-19 13:00:00
categories: jekyll update
output:
  md_document:
    preserve_yaml: TRUE
knit: (function(inputFile, encoding) {
  rmarkdown::render(inputFile, encoding = encoding, output_dir = "../_posts") })
---

The [tidyverse](https://www.tidyverse.org) is one of my favourite suites
of R packages, and its idiomatic approach to data manipulation, analysis
and visualization has heavily influenced how I approach data problems in
R. If there’s one tidyverse package that best embodies these qualities,
my vote is for [`purrr`](https://purrr.tidyverse.org)’s clean approach
to iteration. `purrr` goes well beyond `map` and its variants (check out
[this presentation for a great
sampler](https://hendrikvanb.gitlab.io/slides/purrr_beyond_map.html)),
but it’s been far too easy to ignore anything beyond `purrr`’s simplest
functions.

A while ago at work I had a great opportunity to dive deeper into
`purrr`. While demonstrating the Central Limit Theorem to some
colleagues, I whipped up a simulation of the dice-rolling experiment in
R. I didn’t save the code, but it looked a lot like this:

    library(tidyverse)
    library(purrr)
    library(rlang)
    library(daniel)

    simulate_dice <- function(n, number_of_dice, ...) {
      seq.int(number_of_dice) %>%
        map(~sample(1:6, n, replace = T)) %>%
        as.data.frame(row.names = NULL) %>%
        rowwise() %>%
        summarize(average_of_dice = sum(c_across(where(is.numeric))) / {% raw %}{{number_of_dice}}{% endraw %},
                  number_of_dice = {% raw %}{{number_of_dice}}{% endraw %})
    }

    clt_plotter <- function(df, x_value, facet_value) {
      df %>%
        ggplot() +
        geom_density(aes({% raw %}{{x_value}}{% endraw %}, fill = "1"), colour = "black") +
        scale_fill_daniel() +
        facet_wrap(vars({% raw %}{{facet_value}}{% endraw %})) +
        theme_daniel() +
        guides(fill = F) +
        labs(x = "Sampling distribution of the mean",
             y = "Density")
    }

And here’s the output. The wiggles in the `number_of_dice = 1` facet are
a necessary evil if we want to show the smooth behaviour for larger
numbers of dice without customizing the density plots’ bandwidth and
kernel.

    set.seed(613)
    1:9 %>%
      map_dfr(~simulate_dice(10000, .x)) %>%
      clt_plotter(., x_value = average_of_dice, facet_value = number_of_dice)

![](/assets/old%20version%20output-1.png)

The `simulate_dice` code is functional but hacky:

-   Heavy reliance on dataframes here is a crutch, since we have to use
    the very slow `rowwise` operation to create a column containing the
    mean of each set of thrown dice.
-   Inserting the number of dice thrown as its own column is an ugly way
    to prepare to facet the `clt_plotter` visualization.

We can do better than this, and in the process learn more about the
power of `purrr`.

We’ll start by writing function that doesn’t immediately coerce
everything to a dataframe. Instead, we’ll keep the results as a list as
long as possible. To add the vectors element-wise, we get to avoid
`rowwise` and `mutate`; the answer is `purrr::reduce`. Here, addition is
associative so we don’t need to specify the `.dir` argument for
`reduce`. I also wrote a division function, `vector_division`, to get
the mean of each set of thrown dice, though there’s likely a more
idiomatic way to do this.

    simulate_dice_list <- function(n, number_of_dice, ...) {

      vector_division <- function(x, divisor) {x / divisor}

      seq.int(number_of_dice) %>%
        map(~sample(1:6, n, replace = T)) %>%
        reduce(`+`) %>%
        map(~vector_division(.x, {% raw %}{{number_of_dice}}{% endraw %})) %>%
        unlist()
    }

Now let’s compare the outputs to confirm that they’re identical. Since
the new version doesn’t produce a dataframe, we’ll have to wrangle it
into shape. `enframe` gives us a nice, but nested, dataframe, which we
can further wrangle with `unnest`. As we can see, the results are
identical.

    set.seed(613)
    old_version <- 1:9 %>%
      map_dfr(~simulate_dice(10000, .x))

    set.seed(613)
    new_version <- 1:9 %>%
      map(~simulate_dice_list(10000, .x)) %>%
      enframe(.) %>%
      unnest(value)

    identical(old_version$average_of_dice,
              new_version$value)

    ## [1] TRUE

So which is better? It all comes down to performance, which I assessed
quickly with `microbenchmark`.

    library(microbenchmark)

    set.seed(613)
    benchmark <- microbenchmark(
      old_version = 1:9 %>%
        simulate_dice(100, .x),
      new_version = 1:9 %>%
        simulate_dice_list(100, .x) %>%
        enframe(.) %>%
        unnest(value))

    benchmark

    ## Unit: milliseconds
    ##         expr       min        lq     mean   median       uq       max neval
    ##  old_version 23.097471 36.879219 57.42355 54.53358 72.07341 131.73285   100
    ##  new_version  5.350244  7.995392 15.33288 13.58652 19.48240  67.29409   100

    autoplot(benchmark) +
      theme_daniel()

![](/assets/microbenchmark-1.png)

The difference is quite drastic: `rowwise`, while easy to pluck from the
grab bag of `dplyr` tools, is indeed much slower than using lists.

Here’s the final code. As a bonus, we can avoid the
`summarize(number_of_dice = {% raw %}{{number_of_dice}}{% endraw %})`
code above by naming the input vector with `setNames`.

    set.seed(613)
    setNames(c(1:9), c(1:9)) %>%
      map(~simulate_dice_list(10000, .x)) %>%
      enframe(.) %>%
      unnest(value) %>%
      clt_plotter(., x_value = value, facet_value = name)

![](/assets/new%20version%20output-1.png)

No difference in output, but faster and cleaner. It pays to use `purrr` in conjunction with more than just dataframes!

---
layout: post
title:  "State of the blog"
date:   2020-10-30 07:45:00
categories: jekyll update
---

I've been writing here for a few months now, and I'm happy with the result. While Google says readership is low, that's not unexpected given my hands-off approach to promotion. And from the beginning, my goal wasn't to optimize CTR, but rather to develop a habit of publishing longer-form pieces on a variety of technical subjects. By that metric I think I'm on track.

What's next on the blog? In roughly chronological order:

- A six-month retrospective of the Ergodox EZ and Colemak
- A discussion of mass transit usage in Ottawa
- A new post about second-brain systems and obsidian.md, including quibbles
- Exploring cool [`purrr`](https://purrr.tidyverse.org) features
- More work on the [`daniel`](https://gitlab.com/danielspracklin/daniel) R package
- Tweaks to the blog's CSS

No concrete ETA on these posts, but I'm hoping to tackle most of these in the next six months.

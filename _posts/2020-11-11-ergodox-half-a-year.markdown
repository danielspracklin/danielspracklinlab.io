---
layout: post
title:  "Ergodox EZ&#58; half a year"
date:   2020-11-11 14:27:00
categories: jekyll update
---

_This is the fourth entry in an ongoing series chronicling my adventures with the Ergodox EZ keyboard. Months 1 and 2 are covered [here](https://danielspracklin.gitlab.io/jekyll/update/2020/07/09/ergodox-ez-the-first-two-months.html), months 3 and 4 [here](https://danielspracklin.gitlab.io/jekyll/update/2020/08/28/checking-in-with-the-ergodox-ez-month-four.html), and month 5 [here](https://danielspracklin.gitlab.io/jekyll/update/2020/09/23/ergodox-month-five-ctrl-issues.html)._

I bought my Ergodox EZ just over six months ago and switched layouts from QWERTY to Colemak shortly thereafter. I can't talk meaningfully about one without referencing the other, so although I'll discuss the Ergodox and Colemak separately in this post, I think they're complementary at their heart. If you want to improve your setup, you should think about a split ergonomic keyboard and should explore switching to Colemak. Inversely, if you're considering Colemak, I think you should try it on a split ergonomic keyboard. Onto the details!

## Ergodox EZ

First, some follow-up from the last post, where I discussed some debouncing issues I'd been experiencing. These are mostly gone thanks to QMK debouncing threshold tweaks. Every now and again I'll take the `2u` space key off and readjust it; that seems to help too.

After six months of practice on an Ergodox, I've found myself making frequent comparisons between that board and another type of keyboard: an 88-key one designed to produce sound, not text. If I had to distill my thoughts on the Ergodox into one pithy statement, it might be that _pianists will probably like this keyboard_. Beyond the musically inclined, for whom and when might the Ergodox be a good purchase?

- **If you're tired of pinky strain**. The main benefit of the Ergodox is freeing your pinkies from having to reach for modifier keys. Where to put them instead—thumb cluster or home row?—is an open question, but regardless you'll be saving your pinkies from overwork. I haven't stretched my left pinky for `Ctrl` in months, and I'm better for it!
- **If you want more wrist freedom**. The row stagger on a typical keyboard forces your arms and wrists to point inward. Even if this doesn't bother you, the fact is that you don't have any choice in the matter. Split keyboards like the Ergodox give you the freedom to put the halves wherever you'd like—even [underneath your office chair](https://www.youtube.com/watch?v=Yf-rWiTWfG4) if you want. The downside is a temporary slowdown if you've historically "cheated" hands on the `B` and `Y` keys; you'll either have to retrain your fingers or map some `1.5u` keys for redundancy.
- **If you have reasonably large hands**. You might have trouble comfortably tapping the outer thumb cluster buttons if you've got smaller hands. For reference, if I stretch, I can play a tenth on the white keys of a piano, and I don't have any trouble reaching the `2u` thumb keys. On the other hand, if you have trouble reaching an octave, this is probably not the board for you.
- **If you've got some finger dexterity, or the motivation to develop it**. The switch from row- to column-stagger is not so easy, especially if you've already built up some row-staggered speed. My experience with finger-strength piano exercises convinced me that I'd have the dexterity to retrain my fingers. In the end I was correct in this assessment, but I had to start from scratch to get there: on day one with the Ergodox I went from 120 row-staggered wpm to 7 column-staggered wpm.
- **If you like to tinker**. Some part of my purchase was motivated by the Ergodox's aesthetic—the thing _looks_ cool on a desk. But if you don't enjoy tinkering, you may well tire of continuously improving layouts and tenting angles, and in that sense the keyboard's aesthetics may lead you astray.
- **If you don't mind reading and writing code**. Sooner or later you'll want to try your hand at QMK if you want to derive maximal value from an Ergodox. I'm not a C guy so I've done very little in QMK thus far, but I know I'll have to get my hands dirty at some point. If you're looking for a no-code option, you can get pretty far with Oryx, but the really cool features like tap-dancing are QMK-exclusives.
- **If you're okay with the possibility of retiring it**. There are a wealth of split ergonomic keyboards that have been optimized for specialized use cases (think smaller hands, less finger travel, more aggressive column stagger, etc). The Ergodox can't achieve all these objectives at once, although I'd argue that it's a fine generalist, and so don't expect this to be your endgame. I know I'm already looking at a second split ergo mech board.

## Colemak

There's little doubt in my mind that Colemak is an excellent keyboard layout. After only 6 months in Colemak, I can type more quickly than I could with 25 years' experience in QWERTY, and that's _after_ taking into account all the hacks and mods I'd implemented to boost my QWERTY speed. What's more, typing feels easier in Colemak: there's significantly less finger travel and single-finger typing, and the relatively frequent rolls make it easier to get into a consistent typing rhythm.

For all my praise, I have one damning complaint about Colemak. If you make the switch, it _will_ destroy your QWERTY muscle memory. That means no typing on someone else's keyboard ever, at least not without a strategy to address this. To get around this, I'd suggest that prospective Colemak users consider pairing a layout with a keyboard.[^1] Keep using QWERTY on your daily driver keyboard, pick up a split column-staggered ergonomic keyboard for Colemak purposes, and never mix layouts. This has been my approach since Day 1 of Colemak, and I haven't had any trouble maintaining QWERTY fluency. The many differences in layouts give me enough enough of a cognitive barrier to keep them straight. In fact I typed this entire blog post in QWERTY at pre-Colemak speeds.

[^1]: This logic works the other way around too: if you're planning to switch to a split ergonomic keyboard, it's an ideal time to try a different keyboard layout too. My (biased) recommendation is Colemak, but really anything would do the job.

## Putting it all together

At this point I think I've achieved most of the goals I'd set for myself at the start of this adventure. I made concrete improvements to my ergonomic setup, I learned a new and better keyboard layout, and I broke my old speed barrier. So what's next for me? A couple of things spring to mind. I think there are still speed strides to be be made when programming in Colemak, as well as further layout improvements that would simplify text manipulation. I'm debating picking up a more minimalist keyboard—probably something like a Kyria or an Aysu—and forcing myself to truly commit to the ideal of minimizing finger travel. On the whole, while it feels good to have come reasonably close to mastery, now it's on to the next thing.

<hr>

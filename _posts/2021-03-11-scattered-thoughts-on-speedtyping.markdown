---
layout: post
title:  "Scattered thoughts on speedtyping"
date:   2021-03-11 16:25:00
categories: jekyll update
---

I've been practicing my typing a lot lately; here's what I've got so far.

- Regularly practicing piano scales, arpeggios and triads has increased my
  overall and burst speed by at least 10 wpm.
- Finger strength exercises are critical, especially when using a layout like
  Colemak, where the rolls (`first`) and skip-rolls (`ast, tra`, _etc._) can
  easily trip up weak ring and pinky fingers.
- I like to use the following exercise, which I learned as a child from an old
  Russian piano exercise book. Hold down any three fingers, and alternate
  presses with the other two. My favourite permutation is to hold `124` (thumb,
  index and ring) and let `35` (middle and pinky) vary. Try with both hands
  simultaneously, and see if you can keep your fingers synchronized without
  lifting your ring fingers.
- I still cheat occasionally and hit `Q` with my left ring finger. Maybe I
  haven't been practicing the above exercise enough.
- Always look a word ahead.
- Prioritizing speed at the expense of accuracy is a great way to calcify bad
  habits. Sometimes I can eke out a slightly faster score if I tense my hands
  up, but this generally ends up hurting me in the long run. I usually type
  best when barely feathering the keys. Imagining a sort of digital flow state
  helps.
- I've set myself a fairly ambitious stretch goal this year: hitting 200 wpm on
  a 15-second test. I probably won't reach this.
- There will always be someone faster than me, and that's okay. My only
  real competitor is myself.

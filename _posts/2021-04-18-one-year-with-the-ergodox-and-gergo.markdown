---
layout: post
title:  "One year with the Ergodox&#58; now with Gergo"
date:   2021-04-21 10:00:00
categories: jekyll update
---

_This is the fifth entry in an ongoing series chronicling my adventures with the Ergodox EZ keyboard. Check out [months 1 and 2](https://danielspracklin.gitlab.io/jekyll/update/2020/07/09/ergodox-ez-the-first-two-months.html), [months 3 and 4](https://danielspracklin.gitlab.io/jekyll/update/2020/08/28/checking-in-with-the-ergodox-ez-month-four.html), [month 5](https://danielspracklin.gitlab.io/jekyll/update/2020/09/23/ergodox-month-five-ctrl-issues.html) and [month 6](https://danielspracklin.gitlab.io/jekyll/update/2020/11/11/ergodox-half-a-year.html)._

It's been a year and a few days since the Ergodox EZ landed on my desk. Not a bad time for a... new keyboard.

## Enter the Gergo

I'd always figured I would try a Kyria as my next ergonomic keyboard. It has a bit of everything going for it: fewer keys than most boards, a stylish case and a neat tenting puck design. It's a bit like an Ergodox in a smaller form factor, and so it seemed like a natural fit. But one morning this spring I got to checking keyboard prices and availabilities, and not long after I ended up with the Gboards Gergo on my desk.[^1] The Gergo feels totally different from anything I've ever tried before, and it'll be a fun challenge to get used to it.

[^1]: Rabbit hole: I don't doubt that I'll get a Kyria at some point.

The Ergodox EZ comes with its own ecosystem, including the online Oryx tool that can graphically configure keyboard layouts. There's no such feature with the Gergo, unless you count the less flashy online QMK graphical configurator, but I preferred to jump right into QMK instead. I largely adapted my [existing Ergodox layout](https://gitlab.com/danielspracklin/ergodox-ez/-/blob/master/keymap.c), but had to rethink the thumb clusters, since the Gergo has three fewer keys per thumb cluster, and has only two "thumb-ish" keys where the Ergodox's bottom row is. This wasn't so challenging to work around, though: I moved a couple of the symbols and macros that had occupied the keys in question to other layers, and refined the layers that did exist so I wouldn't need as many `MO` and `TG` keys. This meant removing the QWERTY layer entirely[^2] and tweaking the function layer for easier number typing.

[^2]: Time to commit to learning my passwords in Colemak—better late than never!

[Here's](https://gitlab.com/danielspracklin/gergo/-/blob/master/keymap.c) the layout I've settled on so far. Two of the right-thumb keys are just Discord and DoublePane navigation shortcuts that I'll probably end up moving somewhere else.

## Form factor

Once I got the layout settled, I tried typing on the Gergo, which was completely unlike anything I'd ever typed on before. The first major difference was the feel of the low-profile 20-gram gChoc Kailh switches, which turn out to be incredibly easy to depress when you're used to heavier Cherry Browns. No index finger bump on QWERTY `F` or `J`, but that's not the end of the world in my view. The second difference I noticed was the overall feel of the board: the complete lack of case and deliberate choice to ditch the number row give the Gergo a certain stark hacker-chic vibe. It doesn't feel fragile though; if anything, it feels _fast_, like I could break my old speedtyping PBs with a bit of practice. Another difference is that the Gergo is actually a bit wider than the Ergodox, in the sense that if you line up the near edge of the two boards' outer `1.5u` keys, the Gergo's thumb clusters are slightly farther away than the Ergodox's. It's not a huge difference, but it did trip me up for a bit when hitting the space key.

I've saved the biggest difference, and one that's got me tinkering still, for last. I've become used to the tenting on the Ergodox, so even though I knew the Gergo doesn't come with a tenting kit, it was still a surprise to type on a flat board. I'm not a Fusion 360 wizard so printing my own tenting stand isn't really an option. But my closet is currently housing bins full of several thousand assorted Lego bricks, plates and tiles—really the next best thing. I used some 2x4 sloped bricks to raise the near sides of the Gergo by about 2.25 cm (or roughly 2 bricks), which gives the board a comfortable slope. I also added some plates beneath the back sloped bricks to bring them one-third of a brick higher than the front, so that the board tilts slightly toward me. This worked nicely if I rested my hands on the keyboard; so far so good. But when I typed, the board moved around on the Lego tent, causing a roll along the longitudinal axis that made it hard to depress keys precisely. So I engineered some bracket-style clips that keep the tilting angle roughly stable while typing.[^3] This is an improvement, but the Gergo still has a propensity for scooting around my desk when I type quickly. I can't tell if this because it's so light, or because I'm used to pounding on Cherrys, or both. I thought about building a rigid Lego connector to affix the two halves to each other, but figured this would make it harder to position the two halves exactly how I'd like. I think I'd need an approach that's stable by default but that can be moved with a bit of pressure; maybe there's a way to do this with Lego hydraulics or Technic actuators, as in [this Reddit post](https://www.reddit.com/r/ErgoMechKeyboards/comments/ip3c6m/the_technic_tenting_technique/).

[^3]: I can't for the life of me find the actual part number for this piece on Bricklink; seems like you need to know what a piece is called before you can find it! But you can see them right behind the TRRS cable in the picture.

![The Gergo takes centre stage, while the Ergodox EZ lurks underneath a Komplete Kontrol M32 keyboard, perched atop a(nother) custom Lego stand](/assets/ergodox_gergo_komplete_kontrol.png)

## What of the Ergodox?

For now the Ergodox EZ is my daily keyboard, but I am slowly phasing in the Gergo (mostly on weekends so far) to see how it compares for a variety of use cases. I expect the two boards' layouts to diverge as a function of time. If nothing else, I'll be keeping future me on his toes.

<hr>

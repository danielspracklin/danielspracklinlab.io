---
layout: post
title:  "What's up next"
date:   2022-11-16 19:10:00
categories: jekyll update
---

Awfully quiet on the blog lately, but I've been hard at work on that front, with a monster of a post on the way. Stay tuned to read about how Skelton and Pais' _Team Topologies_, the Make Noise Strega, and the works of Catalan provocateur Albert Serra are all thematically linked.
